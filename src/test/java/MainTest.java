import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.Scanner;

import static org.junit.Assert.assertEquals;

public class MainTest
{
    private final InputStream systemIn = System.in;
    private ByteArrayOutputStream testOut;

    @Before
    public void setUpOutput()
    {
        testOut = new ByteArrayOutputStream();
        System.setOut(new PrintStream(testOut));
    }



    private String getOutput()
    {
        return testOut.toString();
    }

    @After
    public void restoreSystemInputOutput()
    {
        System.setIn(systemIn);
    }

    @Test
    public void someTest()
    {
        String result =
                "{t=000, g=001, s=010, r=011, i=100, n=101,  =110, e=111}\r" +
                " Входная строка: it is test string\r" +
                " Выходная строка: 100 000 110 100 010 110 000 111 010 000 110 010 000 011 100 101 001 \r" +
                " Длина строки: 17\r" +
                " Занимает в памяти: 51 бит\r " +
                "Занимал в памяти: 17 * 8 = 136 бит\r";
        assertEquals(result, result);
    }
}
