public class MyList<T>
{
    private Node<T> head;
    private int size;

    MyList()
    {
        head = new Node<>();
    }

    void add(T item)
    {
        Node<T> node = new Node<>();
        node.item = item;
        size++;

        if (head.item == null)
        {
            head = node;
            return;
        }

        Node<T> rem = head;
        while (rem.next != null) rem = rem.next;

        rem.next = node;
        node.last = rem;
    }

    void set(T item, int index)
    {
        int i = 0;
        Node<T> rem = head;
        while (i < index)
        {
            rem = rem.next;
            i++;
        }
        rem.item = item;
    }

    int size()
    {
        return size;
    }

    T get(int index)
    {
        int i = 0;
        Node<T> rem = head;
        while (i < index)
        {
            rem = rem.next;
            i++;
        }
        return rem.item;
    }

    boolean contains(T object)
    {
        Node<T> rem = head;
        while (rem != null)
        {
            if (object.equals(rem.item))
            {
                return true;
            }
            rem = rem.next;
        }
        return false;
    }

    private static class Node<T>
    {
        T item;
        Node<T> next;
        Node<T> last;
    }

    @Override
    public String toString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        Node<T> rem = head;
        while (rem.next != null)
        {
            sb.append(rem.item);
            if (rem.next.item != null)
            {
                sb.append(", ");
            }
            rem = rem.next;
        }
        sb.append("]");
        return sb.toString();
    }
}