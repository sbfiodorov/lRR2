import java.util.Scanner;

public class Main
{
    public static void main(String[] args)
    {
        Scanner s = new Scanner(System.in);
        System.out.println("Введите строку : ");
        String str = s.nextLine();
        int length = str.length();


        MyHashMap<Character, Integer> map = new MyHashMap<>();

        for (int i = 0; i < str.length(); i++)
        {
            if (map.containsKey(str.charAt(i)))
            {
                map.put(str.charAt(i), map.get(str.charAt(i)) + 1);
            }
            else
            {
                map.put(str.charAt(i), 1);
            }
        }
        map.sortByValues().reversed();

        MyHashMap<String, String> someMap = someMethod(map);
        MyHashMap<Character, String> result = new MyHashMap<>();

        for (int i = 0; i < someMap.size(); i++)
        {
            if (someMap.keyList().get(i).length() == 1)
            {
                result.put(someMap.keyList().get(i).charAt(0),someMap.valueList().get(i));
            }
        }

        StringBuilder resultString = new StringBuilder();
        int size = 0;

        for (int i = 0; i < str.length(); i++)
        {
            resultString.append(result.get(str.charAt(i))).append(" ");
            size += result.get(str.charAt(i)).length();
        }

        System.out.println(result);
        System.out.println("Входная строка: " + str);
        System.out.println("Выходная строка: " + resultString);
        System.out.println("Длина строки: " + length);
        System.out.println("Занимает в памяти: " + size + " бит");
        System.out.println("Занимал в памяти: " + length + " * 8 = " + length * 8 + " бит");
    }

    private static MyHashMap<String, String> someMethod(MyHashMap<Character, Integer> map)
    {
        MyHashMap<Character, Integer> first = new MyHashMap<>();
        MyHashMap<Character, Integer> second = new MyHashMap<>();
        for (int i = 0; i < map.size(); i++)
        {
            if (i % 2 == 0)
                first.put(map.keyList().get(i), map.valueList().get(i));
            else
                second.put(map.keyList().get(i), map.valueList().get(i));
        }
        MyHashMap<String, String> result = new MyHashMap<>();

        StringBuilder firstString = new StringBuilder();
        for (int i = 0; i < first.size(); i++)
        {
            firstString.append(first.keyList().get(i));
        }

        StringBuilder secondString = new StringBuilder();
        for (int i = 0; i < second.size(); i++)
        {
            secondString.append(second.keyList().get(i));
        }
        result.put(firstString.toString(), "0");
        result.put(secondString.toString(), "1");

        if (first.size() > 1)
        {
            MyHashMap<String, String> newResult = someMethod(first);
            for (int i = 0; i < newResult.size(); i++)
            {
                result.put(newResult.keyList().get(i), "0" + newResult.valueList().get(i));
            }
        }
        if (second.size() > 1)
        {
            MyHashMap<String, String> newResult = someMethod(second);
            for (int i = 0; i < newResult.size(); i++)
            {
                result.put(newResult.keyList().get(i), "1" + newResult.valueList().get(i));
            }
        }
        return result;
    }
}