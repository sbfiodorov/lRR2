public class MyHashMap<K, V extends Comparable<V>>
{
    private MyList<K> keys;
    private MyList<V> values;

    MyHashMap()
    {
        this.keys = new MyList<>();
        this.values = new MyList<>();
    }

    void put(K key, V value)
    {
        if (keys.contains(key))
        {
            keys.set(key, getIndex(key));
            values.set(value, getIndex(key));
        }
        else
        {
            keys.add(key);
            values.add(value);
        }
    }

    int size()
    {
        return keys.size();
    }

    boolean containsKey(K key)
    {
        return keys.contains(key);
    }

    MyHashMap<K, V> sortByValues()
    {
        for (int i = 0; i < values.size() - 1; i++)
        {
            for (int j = i + 1; j < values.size(); j++)
            {
                if (values.get(i).compareTo(values.get(j)) > 0)
                {
                    V vRem = values.get(i);
                    values.set(values.get(j), i);
                    values.set(vRem, j);

                    K kRem = keys.get(i);
                    keys.set(keys.get(j), i);
                    keys.set(kRem, j);
                }
            }
        }
        return this;
    }

    void reversed()
    {
        MyList<K> keys = new MyList<>();
        MyList<V> values = new MyList<>();

        for (int i = 0; i < this.keys.size(); i++)
        {
            keys.add(this.keys.get(this.keys.size() - 1 - i));
            values.add(this.values.get(this.values.size() - 1 - i));
        }

        this.keys = keys;
        this.values = values;
    }


    V get(K key)
    {
        for (int i = 0; i < keys.size(); i++)
        {
            if (keys.get(i).equals(key))
                return values.get(i);
        }
        return null;
    }

    private int getIndex(K key)
    {
        for (int i = 0; i < keys.size(); i++)
        {
            if (keys.get(i).equals(key))
                return i;
        }
        return 0;
    }

    MyList<K> keyList()
    {
        return keys;
    }

    MyList<V> valueList()
    {
        return values;
    }

    @Override
    public String toString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("{");
        for (int i = 0; i < keys.size(); i++)
        {
            sb.append(keys.get(i)).append("=").append(values.get(i));
            if (i != keys.size() - 1)
            {
                sb.append(", ");
            }
        }
        sb.append("}");
        return sb.toString();
    }
}